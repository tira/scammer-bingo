#pragma once
int counter = 0;
namespace ScammerBingo {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for mainwindow
	/// </summary>
	public ref class mainwindow : public System::Windows::Forms::Form
	{
	public:
		mainwindow(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~mainwindow()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TextBox^  othernotes;
	protected:

	protected:

	private: System::Windows::Forms::Label^  addnotes;
	private: System::Windows::Forms::Button^  fen;
	private: System::Windows::Forms::Label^  outof17;


	private: System::Windows::Forms::Label^  yourscore;
	private: System::Windows::Forms::Button^  onetimecharge;
	private: System::Windows::Forms::Button^  cmd;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::Button^  iexplore;
	private: System::Windows::Forms::Button^  syskey;

	private: System::Windows::Forms::Button^  hh_h;
	private: System::Windows::Forms::Button^  sss;
	private: System::Windows::Forms::Button^  eventvwr;
	private: System::Windows::Forms::Button^  wirus;
	private: System::Windows::Forms::Button^  networkinfection;
	private: System::Windows::Forms::Button^  weesa;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::GroupBox^  groupBox3;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Button^  eachandeverything;
	private: System::Windows::Forms::GroupBox^  groupBox4;
	private: System::Windows::Forms::Button^  doonething;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  notepad;
	private: System::Windows::Forms::Button^  HIPA;
	private: System::Windows::Forms::Button^  junkfiles;
	private: System::Windows::Forms::Button^  LogMeIn;
	private: System::Windows::Forms::Button^  madarchod;

	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Button^  netsocks;
	private: System::Windows::Forms::Button^  msservices;
	private: System::Windows::Forms::Button^  antispyware;
	private: System::Windows::Forms::Button^  antiwirus;










	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(mainwindow::typeid));
			this->othernotes = (gcnew System::Windows::Forms::TextBox());
			this->addnotes = (gcnew System::Windows::Forms::Label());
			this->fen = (gcnew System::Windows::Forms::Button());
			this->outof17 = (gcnew System::Windows::Forms::Label());
			this->yourscore = (gcnew System::Windows::Forms::Label());
			this->onetimecharge = (gcnew System::Windows::Forms::Button());
			this->cmd = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->netsocks = (gcnew System::Windows::Forms::Button());
			this->msservices = (gcnew System::Windows::Forms::Button());
			this->antispyware = (gcnew System::Windows::Forms::Button());
			this->antiwirus = (gcnew System::Windows::Forms::Button());
			this->LogMeIn = (gcnew System::Windows::Forms::Button());
			this->notepad = (gcnew System::Windows::Forms::Button());
			this->eventvwr = (gcnew System::Windows::Forms::Button());
			this->sss = (gcnew System::Windows::Forms::Button());
			this->syskey = (gcnew System::Windows::Forms::Button());
			this->hh_h = (gcnew System::Windows::Forms::Button());
			this->iexplore = (gcnew System::Windows::Forms::Button());
			this->wirus = (gcnew System::Windows::Forms::Button());
			this->networkinfection = (gcnew System::Windows::Forms::Button());
			this->weesa = (gcnew System::Windows::Forms::Button());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->HIPA = (gcnew System::Windows::Forms::Button());
			this->junkfiles = (gcnew System::Windows::Forms::Button());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->eachandeverything = (gcnew System::Windows::Forms::Button());
			this->groupBox4 = (gcnew System::Windows::Forms::GroupBox());
			this->madarchod = (gcnew System::Windows::Forms::Button());
			this->doonething = (gcnew System::Windows::Forms::Button());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->groupBox3->SuspendLayout();
			this->groupBox4->SuspendLayout();
			this->SuspendLayout();
			// 
			// othernotes
			// 
			this->othernotes->AcceptsTab = true;
			this->othernotes->BackColor = System::Drawing::Color::DodgerBlue;
			this->othernotes->Font = (gcnew System::Drawing::Font(L"Lucida Sans", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->othernotes->ForeColor = System::Drawing::Color::White;
			this->othernotes->Location = System::Drawing::Point(12, 376);
			this->othernotes->Multiline = true;
			this->othernotes->Name = L"othernotes";
			this->othernotes->ScrollBars = System::Windows::Forms::ScrollBars::Vertical;
			this->othernotes->Size = System::Drawing::Size(1034, 97);
			this->othernotes->TabIndex = 0;
			// 
			// addnotes
			// 
			this->addnotes->AutoSize = true;
			this->addnotes->ForeColor = System::Drawing::Color::Transparent;
			this->addnotes->Location = System::Drawing::Point(9, 360);
			this->addnotes->Name = L"addnotes";
			this->addnotes->Size = System::Drawing::Size(85, 13);
			this->addnotes->TabIndex = 1;
			this->addnotes->Text = L"Additional notes:";
			this->addnotes->Click += gcnew System::EventHandler(this, &mainwindow::label1_Click);
			// 
			// fen
			// 
			this->fen->Location = System::Drawing::Point(5, 99);
			this->fen->Name = L"fen";
			this->fen->Size = System::Drawing::Size(150, 75);
			this->fen->TabIndex = 2;
			this->fen->Text = L"Fake English name";
			this->fen->UseVisualStyleBackColor = true;
			this->fen->Click += gcnew System::EventHandler(this, &mainwindow::button1_Click);
			// 
			// outof17
			// 
			this->outof17->AutoSize = true;
			this->outof17->Font = (gcnew System::Drawing::Font(L"Lucida Sans", 20.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->outof17->ForeColor = System::Drawing::Color::White;
			this->outof17->Location = System::Drawing::Point(450, 23);
			this->outof17->Name = L"outof17";
			this->outof17->Size = System::Drawing::Size(80, 32);
			this->outof17->TabIndex = 3;
			this->outof17->Text = L"/  22";
			this->outof17->Click += gcnew System::EventHandler(this, &mainwindow::label1_Click_1);
			// 
			// yourscore
			// 
			this->yourscore->AutoSize = true;
			this->yourscore->Font = (gcnew System::Drawing::Font(L"Lucida Sans", 20.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->yourscore->ForeColor = System::Drawing::Color::White;
			this->yourscore->Location = System::Drawing::Point(403, 23);
			this->yourscore->Name = L"yourscore";
			this->yourscore->Size = System::Drawing::Size(32, 32);
			this->yourscore->TabIndex = 4;
			this->yourscore->Text = L"0";
			this->yourscore->Click += gcnew System::EventHandler(this, &mainwindow::label1_Click_2);
			// 
			// onetimecharge
			// 
			this->onetimecharge->Location = System::Drawing::Point(5, 18);
			this->onetimecharge->Name = L"onetimecharge";
			this->onetimecharge->Size = System::Drawing::Size(150, 75);
			this->onetimecharge->TabIndex = 5;
			this->onetimecharge->Text = L"One time charge";
			this->onetimecharge->UseVisualStyleBackColor = true;
			this->onetimecharge->Click += gcnew System::EventHandler(this, &mainwindow::onetimecharge_Click);
			// 
			// cmd
			// 
			this->cmd->Location = System::Drawing::Point(184, 18);
			this->cmd->Name = L"cmd";
			this->cmd->Size = System::Drawing::Size(150, 75);
			this->cmd->TabIndex = 6;
			this->cmd->Text = L"cmd";
			this->cmd->UseVisualStyleBackColor = true;
			this->cmd->Click += gcnew System::EventHandler(this, &mainwindow::cmd_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Lucida Sans", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label1->ForeColor = System::Drawing::Color::Transparent;
			this->label1->Location = System::Drawing::Point(36, -3);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(77, 18);
			this->label1->TabIndex = 7;
			this->label1->Text = L"Programs";
			this->label1->Click += gcnew System::EventHandler(this, &mainwindow::label1_Click_3);
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->netsocks);
			this->groupBox1->Controls->Add(this->msservices);
			this->groupBox1->Controls->Add(this->antispyware);
			this->groupBox1->Controls->Add(this->antiwirus);
			this->groupBox1->Controls->Add(this->LogMeIn);
			this->groupBox1->Controls->Add(this->notepad);
			this->groupBox1->Controls->Add(this->eventvwr);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Controls->Add(this->sss);
			this->groupBox1->Controls->Add(this->syskey);
			this->groupBox1->Controls->Add(this->hh_h);
			this->groupBox1->Controls->Add(this->iexplore);
			this->groupBox1->Controls->Add(this->cmd);
			this->groupBox1->Location = System::Drawing::Point(535, 11);
			this->groupBox1->Margin = System::Windows::Forms::Padding(2);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Padding = System::Windows::Forms::Padding(2);
			this->groupBox1->Size = System::Drawing::Size(511, 347);
			this->groupBox1->TabIndex = 8;
			this->groupBox1->TabStop = false;
			this->groupBox1->Enter += gcnew System::EventHandler(this, &mainwindow::groupBox1_Enter);
			// 
			// netsocks
			// 
			this->netsocks->Location = System::Drawing::Point(356, 261);
			this->netsocks->Name = L"netsocks";
			this->netsocks->Size = System::Drawing::Size(150, 75);
			this->netsocks->TabIndex = 18;
			this->netsocks->Text = L"Network Sockets";
			this->netsocks->UseVisualStyleBackColor = true;
			this->netsocks->Click += gcnew System::EventHandler(this, &mainwindow::netsocks_Click);
			// 
			// msservices
			// 
			this->msservices->Location = System::Drawing::Point(356, 180);
			this->msservices->Name = L"msservices";
			this->msservices->Size = System::Drawing::Size(150, 75);
			this->msservices->TabIndex = 17;
			this->msservices->Text = L"MS Services";
			this->msservices->UseVisualStyleBackColor = true;
			this->msservices->Click += gcnew System::EventHandler(this, &mainwindow::msservices_Click);
			// 
			// antispyware
			// 
			this->antispyware->Location = System::Drawing::Point(356, 99);
			this->antispyware->Name = L"antispyware";
			this->antispyware->Size = System::Drawing::Size(150, 75);
			this->antispyware->TabIndex = 16;
			this->antispyware->Text = L"Anti-spyware";
			this->antispyware->UseVisualStyleBackColor = true;
			this->antispyware->Click += gcnew System::EventHandler(this, &mainwindow::antispyware_Click);
			// 
			// antiwirus
			// 
			this->antiwirus->Location = System::Drawing::Point(356, 18);
			this->antiwirus->Name = L"antiwirus";
			this->antiwirus->Size = System::Drawing::Size(150, 75);
			this->antiwirus->TabIndex = 15;
			this->antiwirus->Text = L"Anti-wirus";
			this->antiwirus->UseVisualStyleBackColor = true;
			this->antiwirus->Click += gcnew System::EventHandler(this, &mainwindow::antiwirus_Click);
			// 
			// LogMeIn
			// 
			this->LogMeIn->Location = System::Drawing::Point(184, 261);
			this->LogMeIn->Name = L"LogMeIn";
			this->LogMeIn->Size = System::Drawing::Size(150, 75);
			this->LogMeIn->TabIndex = 14;
			this->LogMeIn->Text = L"LogMeIn";
			this->LogMeIn->UseVisualStyleBackColor = true;
			this->LogMeIn->Click += gcnew System::EventHandler(this, &mainwindow::LogMeIn_Click);
			// 
			// notepad
			// 
			this->notepad->Location = System::Drawing::Point(5, 261);
			this->notepad->Name = L"notepad";
			this->notepad->Size = System::Drawing::Size(150, 75);
			this->notepad->TabIndex = 13;
			this->notepad->Text = L"notepad";
			this->notepad->UseVisualStyleBackColor = true;
			this->notepad->Click += gcnew System::EventHandler(this, &mainwindow::notepad_Click);
			// 
			// eventvwr
			// 
			this->eventvwr->Location = System::Drawing::Point(184, 180);
			this->eventvwr->Name = L"eventvwr";
			this->eventvwr->Size = System::Drawing::Size(150, 75);
			this->eventvwr->TabIndex = 12;
			this->eventvwr->Text = L"eventvwr";
			this->eventvwr->UseVisualStyleBackColor = true;
			this->eventvwr->Click += gcnew System::EventHandler(this, &mainwindow::button2_Click);
			// 
			// sss
			// 
			this->sss->Location = System::Drawing::Point(5, 180);
			this->sss->Name = L"sss";
			this->sss->Size = System::Drawing::Size(150, 75);
			this->sss->TabIndex = 9;
			this->sss->Text = L"Super Anti-Spyware";
			this->sss->UseVisualStyleBackColor = true;
			this->sss->Click += gcnew System::EventHandler(this, &mainwindow::sss_Click);
			// 
			// syskey
			// 
			this->syskey->Location = System::Drawing::Point(184, 99);
			this->syskey->Name = L"syskey";
			this->syskey->Size = System::Drawing::Size(150, 75);
			this->syskey->TabIndex = 11;
			this->syskey->Text = L"syskey";
			this->syskey->UseVisualStyleBackColor = true;
			this->syskey->Click += gcnew System::EventHandler(this, &mainwindow::syskey_Click);
			// 
			// hh_h
			// 
			this->hh_h->Location = System::Drawing::Point(5, 99);
			this->hh_h->Name = L"hh_h";
			this->hh_h->Size = System::Drawing::Size(150, 75);
			this->hh_h->TabIndex = 10;
			this->hh_h->Text = L"hh h";
			this->hh_h->UseVisualStyleBackColor = true;
			this->hh_h->Click += gcnew System::EventHandler(this, &mainwindow::hh_h_Click);
			// 
			// iexplore
			// 
			this->iexplore->Location = System::Drawing::Point(5, 18);
			this->iexplore->Name = L"iexplore";
			this->iexplore->Size = System::Drawing::Size(150, 75);
			this->iexplore->TabIndex = 9;
			this->iexplore->Text = L"iexplore";
			this->iexplore->UseVisualStyleBackColor = true;
			this->iexplore->Click += gcnew System::EventHandler(this, &mainwindow::iexplore_Click);
			// 
			// wirus
			// 
			this->wirus->Location = System::Drawing::Point(5, 19);
			this->wirus->Name = L"wirus";
			this->wirus->Size = System::Drawing::Size(150, 75);
			this->wirus->TabIndex = 9;
			this->wirus->Text = L"Wirus";
			this->wirus->UseVisualStyleBackColor = true;
			this->wirus->Click += gcnew System::EventHandler(this, &mainwindow::wirus_Click);
			// 
			// networkinfection
			// 
			this->networkinfection->Location = System::Drawing::Point(184, 18);
			this->networkinfection->Name = L"networkinfection";
			this->networkinfection->Size = System::Drawing::Size(150, 75);
			this->networkinfection->TabIndex = 10;
			this->networkinfection->Text = L"Network infection";
			this->networkinfection->UseVisualStyleBackColor = true;
			this->networkinfection->Click += gcnew System::EventHandler(this, &mainwindow::networkinfection_Click);
			// 
			// weesa
			// 
			this->weesa->Location = System::Drawing::Point(184, 18);
			this->weesa->Name = L"weesa";
			this->weesa->Size = System::Drawing::Size(150, 75);
			this->weesa->TabIndex = 11;
			this->weesa->Text = L"Weysa card";
			this->weesa->UseVisualStyleBackColor = true;
			this->weesa->Click += gcnew System::EventHandler(this, &mainwindow::weesa_Click);
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->label2);
			this->groupBox2->Controls->Add(this->weesa);
			this->groupBox2->Controls->Add(this->onetimecharge);
			this->groupBox2->Location = System::Drawing::Point(192, 250);
			this->groupBox2->Margin = System::Windows::Forms::Padding(2);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Padding = System::Windows::Forms::Padding(2);
			this->groupBox2->Size = System::Drawing::Size(339, 108);
			this->groupBox2->TabIndex = 13;
			this->groupBox2->TabStop = false;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Lucida Sans", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label2->ForeColor = System::Drawing::Color::Transparent;
			this->label2->Location = System::Drawing::Point(36, -3);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(71, 18);
			this->label2->TabIndex = 7;
			this->label2->Text = L"Payment";
			this->label2->Click += gcnew System::EventHandler(this, &mainwindow::label2_Click);
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->HIPA);
			this->groupBox3->Controls->Add(this->junkfiles);
			this->groupBox3->Controls->Add(this->label3);
			this->groupBox3->Controls->Add(this->networkinfection);
			this->groupBox3->Controls->Add(this->wirus);
			this->groupBox3->Location = System::Drawing::Point(192, 58);
			this->groupBox3->Margin = System::Windows::Forms::Padding(2);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Padding = System::Windows::Forms::Padding(2);
			this->groupBox3->Size = System::Drawing::Size(339, 182);
			this->groupBox3->TabIndex = 14;
			this->groupBox3->TabStop = false;
			// 
			// HIPA
			// 
			this->HIPA->Location = System::Drawing::Point(184, 99);
			this->HIPA->Name = L"HIPA";
			this->HIPA->Size = System::Drawing::Size(150, 75);
			this->HIPA->TabIndex = 12;
			this->HIPA->Text = L"Hacked IP Address";
			this->HIPA->UseVisualStyleBackColor = true;
			this->HIPA->Click += gcnew System::EventHandler(this, &mainwindow::HIPA_Click);
			// 
			// junkfiles
			// 
			this->junkfiles->Location = System::Drawing::Point(5, 100);
			this->junkfiles->Name = L"junkfiles";
			this->junkfiles->Size = System::Drawing::Size(150, 75);
			this->junkfiles->TabIndex = 11;
			this->junkfiles->Text = L"Junk files";
			this->junkfiles->UseVisualStyleBackColor = true;
			this->junkfiles->Click += gcnew System::EventHandler(this, &mainwindow::junkfiles_Click);
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"Lucida Sans", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label3->ForeColor = System::Drawing::Color::Transparent;
			this->label3->Location = System::Drawing::Point(36, -3);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(82, 18);
			this->label3->TabIndex = 7;
			this->label3->Text = L"Infections";
			// 
			// eachandeverything
			// 
			this->eachandeverything->Location = System::Drawing::Point(5, 18);
			this->eachandeverything->Name = L"eachandeverything";
			this->eachandeverything->Size = System::Drawing::Size(150, 75);
			this->eachandeverything->TabIndex = 15;
			this->eachandeverything->Text = L"Each and everything";
			this->eachandeverything->UseVisualStyleBackColor = true;
			this->eachandeverything->Click += gcnew System::EventHandler(this, &mainwindow::eachandeverything_Click);
			// 
			// groupBox4
			// 
			this->groupBox4->Controls->Add(this->madarchod);
			this->groupBox4->Controls->Add(this->doonething);
			this->groupBox4->Controls->Add(this->label4);
			this->groupBox4->Controls->Add(this->eachandeverything);
			this->groupBox4->Controls->Add(this->button2);
			this->groupBox4->Controls->Add(this->fen);
			this->groupBox4->Location = System::Drawing::Point(12, 11);
			this->groupBox4->Margin = System::Windows::Forms::Padding(2);
			this->groupBox4->Name = L"groupBox4";
			this->groupBox4->Padding = System::Windows::Forms::Padding(2);
			this->groupBox4->Size = System::Drawing::Size(160, 347);
			this->groupBox4->TabIndex = 15;
			this->groupBox4->TabStop = false;
			// 
			// madarchod
			// 
			this->madarchod->Location = System::Drawing::Point(5, 261);
			this->madarchod->Name = L"madarchod";
			this->madarchod->Size = System::Drawing::Size(150, 75);
			this->madarchod->TabIndex = 17;
			this->madarchod->Text = L"Hindi swearing";
			this->madarchod->UseVisualStyleBackColor = true;
			this->madarchod->Click += gcnew System::EventHandler(this, &mainwindow::button1_Click_1);
			// 
			// doonething
			// 
			this->doonething->Location = System::Drawing::Point(5, 180);
			this->doonething->Name = L"doonething";
			this->doonething->Size = System::Drawing::Size(150, 75);
			this->doonething->TabIndex = 16;
			this->doonething->Text = L"Do one thing";
			this->doonething->UseVisualStyleBackColor = true;
			this->doonething->Click += gcnew System::EventHandler(this, &mainwindow::doonething_Click);
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Font = (gcnew System::Drawing::Font(L"Lucida Sans", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label4->ForeColor = System::Drawing::Color::Transparent;
			this->label4->Location = System::Drawing::Point(36, -3);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(63, 18);
			this->label4->TabIndex = 7;
			this->label4->Text = L"Speech";
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(184, 18);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(150, 75);
			this->button2->TabIndex = 10;
			this->button2->Text = L"Network infection";
			this->button2->UseVisualStyleBackColor = true;
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Font = (gcnew System::Drawing::Font(L"Lucida Sans", 18, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label5->ForeColor = System::Drawing::Color::White;
			this->label5->Location = System::Drawing::Point(192, 11);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(192, 28);
			this->label5->TabIndex = 16;
			this->label5->Text = L"Scammer Bingo";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Font = (gcnew System::Drawing::Font(L"Lucida Sans", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label6->ForeColor = System::Drawing::Color::White;
			this->label6->Location = System::Drawing::Point(411, 8);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(48, 15);
			this->label6->TabIndex = 17;
			this->label6->Text = L"Score:";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Font = (gcnew System::Drawing::Font(L"Lucida Sans", 6.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label7->ForeColor = System::Drawing::Color::White;
			this->label7->Location = System::Drawing::Point(189, 42);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(208, 13);
			this->label7->TabIndex = 18;
			this->label7->Text = L"https://github.com/itstiraboschi/scammer-bingo";
			this->label7->Click += gcnew System::EventHandler(this, &mainwindow::label7_Click);
			// 
			// mainwindow
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::Color::DodgerBlue;
			this->ClientSize = System::Drawing::Size(1055, 485);
			this->Controls->Add(this->label7);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->groupBox4);
			this->Controls->Add(this->groupBox3);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->yourscore);
			this->Controls->Add(this->outof17);
			this->Controls->Add(this->addnotes);
			this->Controls->Add(this->othernotes);
			this->Controls->Add(this->groupBox1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->MaximizeBox = false;
			this->MinimizeBox = false;
			this->Name = L"mainwindow";
			this->Text = L"Scammer Bingo";
			this->Load += gcnew System::EventHandler(this, &mainwindow::mainwindow_Load);
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->groupBox3->ResumeLayout(false);
			this->groupBox3->PerformLayout();
			this->groupBox4->ResumeLayout(false);
			this->groupBox4->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void label1_Click(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		this->fen->Enabled = false;
		counter++;
		yourscore->Text = counter.ToString();
	}
	private: System::Void label1_Click_1(System::Object^  sender, System::EventArgs^  e) {
	}
private: System::Void label1_Click_2(System::Object^  sender, System::EventArgs^  e) {
	}
private: System::Void groupBox1_Enter(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void onetimecharge_Click(System::Object^  sender, System::EventArgs^  e) {
	this->onetimecharge->Enabled = false;
	counter++;
	yourscore->Text = counter.ToString();
}
private: System::Void label1_Click_3(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
	this->eventvwr->Enabled = false;
	counter++;
	yourscore->Text = counter.ToString();
}
private: System::Void label2_Click(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void label5_Click(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void label7_Click(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void mainwindow_Load(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void eachandeverything_Click(System::Object^  sender, System::EventArgs^  e) {
	this->eachandeverything->Enabled = false;
	counter++;
	yourscore->Text = counter.ToString();
}
private: System::Void doonething_Click(System::Object^  sender, System::EventArgs^  e) {
	this->doonething->Enabled = false;
	counter++;
	yourscore->Text = counter.ToString();
}
private: System::Void button1_Click_1(System::Object^  sender, System::EventArgs^  e) {
	this->madarchod->Enabled = false;
	counter++;
	yourscore->Text = counter.ToString();
}
private: System::Void wirus_Click(System::Object^  sender, System::EventArgs^  e) {
	this->wirus->Enabled = false;
	counter++;
	yourscore->Text = counter.ToString();
}
private: System::Void junkfiles_Click(System::Object^  sender, System::EventArgs^  e) {
	this->junkfiles->Enabled = false;
	counter++;
	yourscore->Text = counter.ToString();
}
private: System::Void networkinfection_Click(System::Object^  sender, System::EventArgs^  e) {
	this->networkinfection->Enabled = false;
	counter++;
	yourscore->Text = counter.ToString();
}
private: System::Void HIPA_Click(System::Object^  sender, System::EventArgs^  e) {
	this->HIPA->Enabled = false;
	counter++;
	yourscore->Text = counter.ToString();
}
private: System::Void weesa_Click(System::Object^  sender, System::EventArgs^  e) {
	this->weesa->Enabled = false;
	counter++;
	yourscore->Text = counter.ToString();
}
private: System::Void iexplore_Click(System::Object^  sender, System::EventArgs^  e) {
	this->iexplore->Enabled = false;
	counter++;
	yourscore->Text = counter.ToString();
}
private: System::Void cmd_Click(System::Object^  sender, System::EventArgs^  e) {
	this->cmd->Enabled = false;
	counter++;
	yourscore->Text = counter.ToString();
}
private: System::Void antiwirus_Click(System::Object^  sender, System::EventArgs^  e) {
	this->antiwirus->Enabled = false;
	counter++;
	yourscore->Text = counter.ToString();
}
private: System::Void hh_h_Click(System::Object^  sender, System::EventArgs^  e) {
	this->hh_h->Enabled = false;
	counter++;
	yourscore->Text = counter.ToString();
}
private: System::Void syskey_Click(System::Object^  sender, System::EventArgs^  e) {
	if (syskey->Enabled) {
		counter++;
		yourscore->Text = counter.ToString();
	}
	this->syskey->Enabled = false;
}
private: System::Void antispyware_Click(System::Object^  sender, System::EventArgs^  e) {
	this->antispyware->Enabled = false;
	counter++;
	yourscore->Text = counter.ToString();
}
private: System::Void sss_Click(System::Object^  sender, System::EventArgs^  e) {
	this->sss->Enabled = false;
	counter++;
	yourscore->Text = counter.ToString();
}
private: System::Void msservices_Click(System::Object^  sender, System::EventArgs^  e) {
	this->msservices->Enabled = false;
	counter++;
	yourscore->Text = counter.ToString();
}
private: System::Void notepad_Click(System::Object^  sender, System::EventArgs^  e) {
	this->notepad->Enabled = false;
	counter++;
	yourscore->Text = counter.ToString();
}
private: System::Void LogMeIn_Click(System::Object^  sender, System::EventArgs^  e) {
	this->LogMeIn->Enabled = false;
	counter++;
	yourscore->Text = counter.ToString();
}
private: System::Void netsocks_Click(System::Object^  sender, System::EventArgs^  e) {
	this->netsocks->Enabled = false;
	counter++;
	yourscore->Text = counter.ToString();
}
};
}
